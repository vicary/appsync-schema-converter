# AppSync Schema Converter

The sole purpose of this package is to convert modern GraphQL schemas into AppSync compatible version.

1. `printSchema()` is a copy of `graphql@^15/utilities/printSchema.js` with AppSync specific options added.
2. `convertSchemas(schemas: [string])` takes an array of GraphQL SDL string and converts them into one single AppSync comptaible schema.

## Upgrade from 1.x

All exposed functions are now synchronize, async/await was found unnecessary.

# Usage

## CLI

This package supports direct shell invocation via npx:

```bash
npx appsync-schmea-converter **/*.graphql
```

## Serverless Framework

This package also made with [`serverless-appsync-plugin`](https://www.npmjs.com/package/serverless-appsync-plugin) in mind, especially useful when [`merge-graphql-schemas`](https://www.npmjs.com/package/merge-graphql-schemas) was in your stack.

You make use of [variables in JavaScript](https://serverless.com/framework/docs/providers/aws/guide/variables/#reference-variables-in-javascript-files) and write a little script to merge schemas into AppSync compatible one.

Based on your `serverless-appsync-plugin` settings, change this line in your `serverless.yml`.

```YAML
custom:
  appSync:
    schema: ${file(schema.js):compile}
```

Then read and convert your schemas in `schema.js@compile`.

```javascript
const glob = require("fast-glob");
const { promises: fs } = require("fs");
const { convertSchemas } = require("appsync-schema-converter");

const SCHEMA_PATH = "./schema.graphql";

module.exports.compile = async (_) => {
  let schemas;

  schemas = await glob(`${__dirname}/schemas/**/*.graphql`);
  schemas = await Promise.all(schemas.map((schema) => fs.readFile(schema, { encoding: "utf-8" })));
  schemas = convertSchemas(schemas, {
    commentDescriptions: true,
    includeDirectives: true,
    includeEnumDescriptions: false,
    interfaceSeparator: ", ",
  });
  // Or use the simplified version: convertAppSyncSchemas(schemas);

  await fs.writeFile(SCHEMA_PATH, schemas);

  return SCHEMA_PATH;
};
```

### Missing comments?

You may notice that after deploying to AppSync, schema comments are still
nowhere to be found.

It appears that `serverless-appsync-plugin@<=1.11.3` simply removes ALL schema
comments when building the CloudFormation stack JSON. Even if this plugin
correctly converts the schemas.

If you are feeling extra adventurous, you may skip all the above and use my fork
[serverless-appsync-plugin#modern-schema](https://github.com/vicary/serverless-appsync-plugin/tree/modern-schema)
instead. It has this package fully integrated into the plugin itself. The behavior is expected to largely stay the same, with the the comment-stripping
logic replaced with my workarounds for AppSync schema syntax.

I try to kept all the unit tests passing, and my team is already using it in production.

# Funding

If you find this project useful, please [chip in](https://github.com/sponsors/vicary) so I can keep making it even better!
