// printSchema.js: A modification of graphql@15.5.0/utilities/printSchema.js.
"use strict";

const {
  astFromValue,
  DEFAULT_DEPRECATION_REASON,
  GraphQLString,
  isEnumType,
  isInputObjectType,
  isInterfaceType,
  isIntrospectionType,
  isObjectType,
  isScalarType,
  isSpecifiedDirective,
  isSpecifiedScalarType,
  isUnionType,
  print,
  printIntrospectionSchema,
} = require("graphql");

const _invariant = require("graphql/jsutils/invariant");
const _inspect = require("graphql/jsutils/inspect");
const { printBlockString } = require("graphql/language/blockString");

module.exports.isDefinedType = isDefinedType;
module.exports.printIntrospectionSchema = printIntrospectionSchema;
module.exports.printSchema = printSchema;
module.exports.printType = printType;

/**
 * Accepts options as a second argument:
 *
 *    - commentDescriptions:
 *        Provide true to use preceding comments as the description.
 *    - includeDirectives:
 *        Provide true to also print directives.
 *    - includeEnumDescriptions:
 *        Provide true to include enum descriptions, default to true.
 *    - directiveFilter:
 *        Filters for directives to be printed.
 *    - typeFilter:
 *        Filters for types to be printed.
 */
function printSchema(
  schema,
  { directiveFilter = (n) => !isSpecifiedDirective(n), typeFilter = isDefinedType, ...options },
) {
  return printFilteredSchema(schema, directiveFilter, typeFilter, options);
}

function isDefinedType(type) {
  return !isSpecifiedScalarType(type) && !isIntrospectionType(type);
}

function printFilteredSchema(schema, directiveFilter, typeFilter, options) {
  var directives = schema.getDirectives().filter(directiveFilter);
  var types = Object.values(schema.getTypeMap()).filter(typeFilter);

  return (
    [printSchemaDefinition(schema)]
      .concat(
        directives.map((directive) => printDirective(directive, options)),
        types.map((type) => printType(type, options)),
      )
      .filter(Boolean)
      .join("\n\n") + "\n"
  );
}

function printSchemaDefinition(schema) {
  if (schema.description == null && isSchemaOfCommonNames(schema)) {
    return;
  }

  var operationTypes = [];
  var queryType = schema.getQueryType();

  if (queryType) {
    operationTypes.push("  query: ".concat(queryType.name));
  }

  var mutationType = schema.getMutationType();

  if (mutationType) {
    operationTypes.push("  mutation: ".concat(mutationType.name));
  }

  var subscriptionType = schema.getSubscriptionType();

  if (subscriptionType) {
    operationTypes.push("  subscription: ".concat(subscriptionType.name));
  }

  return printDescription({}, schema) + "schema {\n".concat(operationTypes.join("\n"), "\n}");
}

/**
 * GraphQL schema define root types for each type of operation. These types are
 * the same as any other type and can be named in any manner, however there is
 * a common naming convention:
 *
 *   schema {
 *     query: Query
 *     mutation: Mutation
 *   }
 *
 * When using this naming convention, the schema description can be omitted.
 */
function isSchemaOfCommonNames(schema) {
  var queryType = schema.getQueryType();

  if (queryType && queryType.name !== "Query") {
    return false;
  }

  var mutationType = schema.getMutationType();

  if (mutationType && mutationType.name !== "Mutation") {
    return false;
  }

  var subscriptionType = schema.getSubscriptionType();

  if (subscriptionType && subscriptionType.name !== "Subscription") {
    return false;
  }

  return true;
}

function printType(type, options) {
  if (isScalarType(type)) {
    return printScalar(type, options);
  } else if (isObjectType(type)) {
    return printObject(type, options);
  } else if (isInterfaceType(type)) {
    return printInterface(type, options);
  } else if (isUnionType(type)) {
    return printUnion(type, options);
  } else if (isEnumType(type)) {
    return printEnum(type, options);
  } // istanbul ignore else (See: 'https://github.com/graphql/graphql-js/issues/2618')

  if (isInputObjectType(type)) {
    return printInputObject(type, options);
  } // istanbul ignore next (Not reachable. All possible types have been considered)

  _invariant("Unexpected type: " + _inspect(type));
}

function printScalar(type, options) {
  return printDescription(options, type) + "scalar ".concat(type.name) + printSpecifiedByUrl(type);
}

function printImplementedInterfaces(options, type) {
  var separator = (options && options.interfaceSeparator) || " & ";
  // graphql 14 does not support `.getInterfaces()`
  var interfaces = typeof type.getInterfaces === "function" ? type.getInterfaces() : [];
  var uniqueInterfaces = interfaces.reduce((prev, value) => {
    if (!prev.includes(value)) {
      prev.push(value);
    }

    return prev;
  }, []);

  return interfaces.length ? " implements " + uniqueInterfaces.map((i) => i.name).join(separator) : "";
}

function printObject(type, options) {
  return (
    printDescription(options, type) +
    "type ".concat(type.name) +
    printImplementedInterfaces(options, type) +
    printObjectDirectives(type, options) +
    printFields(options, type)
  );
}

function printObjectDirectives(type, options) {
  if (options && options.includeDirectives === true) {
    const directives = type.astNode.directives.map(print).join(" ").trim();
    if (directives) {
      return " " + directives;
    }
  }

  return "";
}

function printInterface(type, options) {
  return (
    printDescription(options, type) +
    "interface ".concat(type.name) +
    printImplementedInterfaces(options, type) +
    printFields(options, type)
  );
}

function printUnion(type, options) {
  var types = type.getTypes();
  var possibleTypes = types.length ? " = " + types.join(" | ") : "";

  return printDescription(options, type) + "union " + type.name + possibleTypes;
}

function printEnum(type, options) {
  var includeDescriptions = !options || options.includeEnumDescriptions;
  var values = type
    .getValues()
    .map(
      (value, i) =>
        (includeDescriptions ? printDescription(options, value, "  ", !i) : "") +
        "  " +
        value.name +
        printDeprecated(value.deprecationReason),
    );

  return (includeDescriptions ? printDescription(options, type) : "") + "enum ".concat(type.name) + printBlock(values);
}

function printInputObject(type, options) {
  var fields = Object.values(type.getFields()).map(
    (f, i) => printDescription(options, f, "  ", !i) + "  " + printInputValue(f),
  );

  return printDescription(options, type) + "input ".concat(type.name) + printBlock(fields);
}

function printFields(options, type) {
  var includeDirectives = options && options.includeDirectives === true;
  var fields = Object.values(type.getFields()).map((f, i) => {
    if (includeDirectives && f && f.astNode && f.astNode.directives && f.astNode.directives.length) {
      f.astNode.description = null;

      var fieldDecl = print(f.astNode);

      return printDescription(options, f, "  ", !i) + "  " + fieldDecl.replace(/\n/g, "\n  ");
    }

    return (
      printDescription(options, f, "  ", !i) +
      "  " +
      f.name +
      printArgs(options, f.args, "  ") +
      ": " +
      String(f.type) +
      printDeprecated(f.deprecationReason)
    );
  });

  return printBlock(fields);
}

function printBlock(items) {
  return items.length !== 0 ? " {\n" + items.join("\n") + "\n}" : "";
}

function printArgs(options, args) {
  var indentation = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : "";

  if (args.length === 0) {
    return "";
  } // If every arg does not have a description, print them on one line.

  if (
    args.every(function (arg) {
      return !arg.description;
    })
  ) {
    return "(" + args.map(printInputValue).join(", ") + ")";
  }

  return (
    "(\n" +
    args
      .map(function (arg, i) {
        return printDescription(options, arg, "  " + indentation, !i) + "  " + indentation + printInputValue(arg);
      })
      .join("\n") +
    "\n" +
    indentation +
    ")"
  );
}

function printInputValue(arg) {
  var defaultAST = astFromValue(arg.defaultValue, arg.type);
  var argDecl = arg.name + ": " + String(arg.type);

  if (defaultAST) {
    argDecl += " = ".concat(print(defaultAST));
  }

  return argDecl + printDeprecated(arg.deprecationReason);
}

function printDirective(directive, options) {
  return (
    printDescription(options, directive) +
    "directive @" +
    directive.name +
    printArgs(options, directive.args) +
    (directive.isRepeatable ? " repeatable" : "") +
    " on " +
    directive.locations.join(" | ")
  );
}

function printDeprecated(reason) {
  if (reason == null) {
    return "";
  }

  var reasonAST = astFromValue(reason, GraphQLString);

  if (reasonAST && reason !== DEFAULT_DEPRECATION_REASON) {
    return " @deprecated(reason: " + print(reasonAST) + ")";
  }

  return " @deprecated";
}

function printSpecifiedByUrl(scalar) {
  if (scalar.specifiedByUrl == null) {
    return "";
  }

  var url = scalar.specifiedByUrl;
  var urlAST = astFromValue(url, GraphQLString);
  urlAST || _invariant("Unexpected null value returned from `astFromValue` for specifiedByUrl");
  return " @specifiedBy(url: " + print(urlAST) + ")";
}

function printDescription(options, def) {
  var indentation = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : "";
  var firstInBlock = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : true;
  var description = def.description;

  if (description == null) {
    return "";
  }

  if ((options === null || options === void 0 ? void 0 : options.commentDescriptions) === true) {
    return printDescriptionWithComments(description, indentation, firstInBlock);
  }

  var preferMultipleLines = description.length > 70;
  var blockString = printBlockString(description, "", preferMultipleLines);
  var prefix = indentation && !firstInBlock ? "\n" + indentation : indentation;
  return prefix + blockString.replace(/\n/g, "\n" + indentation) + "\n";
}

function printDescriptionWithComments(description, indentation, firstInBlock) {
  var prefix = indentation && !firstInBlock ? "\n" : "";
  var comment = description
    .split("\n")
    .map(function (line) {
      return indentation + (line !== "" ? "# " + line : "#");
    })
    .join("\n");
  return prefix + comment + "\n";
}
