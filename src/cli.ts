#! /usr/bin/env node

import { program } from "commander";
import { promises as fs, readFileSync } from "fs";
import { resolve } from "path";
import { convertAppSyncSchemas } from "./";

const { version } = JSON.parse(readFileSync(resolve(__dirname, "../package.json"), "utf-8"));

program
  .version(version)
  .argument("<schemas...>", "GraphQL schema files to be converted.")
  .action(async (schemas: string[]) => {
    const files = await Promise.all(schemas.map((schema) => fs.readFile(schema, "utf-8")));

    console.log(convertAppSyncSchemas(files));
  })
  .showHelpAfterError()
  .parse();
