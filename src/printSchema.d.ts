import {
  GraphQLEnumType,
  GraphQLInputObjectType,
  GraphQLInterfaceType,
  GraphQLObjectType,
  GraphQLScalarType,
  GraphQLSchema,
  GraphQLType,
  GraphQLUnionType,
} from "graphql";

export type { printIntrospectionSchema } from "graphql";

export type PrintOptions = {
  /**
   * Option copied from printSchema, `true` to use hash comments as descriptions.
   */
  commentDescriptions?: Boolean;

  /**
   * `true` to include directives into printed result, this option exists
   * because the original `schemaPrinter.js` has it.
   */
  includeDirectives?: Boolean;

  /**
   * AppSync does not supprt descriptions in enum, specify `false` here to skip
   * them. Defaults `true`.
   */
  includeEnumDescriptions?: Boolean;

  /**
   * AppSync uses an official separator for multiple interfaces ", ", defaults
   * to the official separator " & ".
   */
  interfaceSeparator?: string;

  directiveFilter?: (directive: any) => Boolean;
  typeFilter?: (type: any) => Boolean;
};

export function printSchema(schema: GraphQLSchema, options: PrintOptions): string;

export function printType(
  type:
    | GraphQLEnumType
    | GraphQLInputObjectType
    | GraphQLInterfaceType
    | GraphQLObjectType
    | GraphQLScalarType
    | GraphQLUnionType,
  options: PrintOptions,
): string;

export function isDefinedType(type: GraphQLType): boolean;
