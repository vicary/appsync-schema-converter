import { buildSchema, convertAppSyncSchemas, convertSchemas } from ".";

const baseSchema = `
"""
type description
"""
type Query {
  "field description"
  testBaseQuery: String
}

"this is a mutation"
type Mutation {
  testMutation: String
}
`;

const extendedSchema = `
extend type Query {
  """
  description on extended type
  """
  testExtendQuery: String
}

type Subscription {
  testUpdate: String
  @aws_subscribe(mutations: ["testMutation"])
}
`;

const schemaWithDirectives = `
directive @foo on FIELD_DEFINITION

extend type Query
{
  """
  This field is deprecated.
  """
  foo: String
    @aws_api_key
    @aws_iam
    @aws_oidc
    @aws_cognito_user_pools
    @aws_auth(cognito_groups: ["users"])
    @deprecated(reason: "test")
    @foo
}
`;

const schemaWithInterfaces = `
interface A {
  a: String
}

interface B {
  b: String
}

type C implements A & B {
  a: String
  b: String
}
`;

const schemaWithDuplicatedInterfacesViaExtension = `
interface A {
  a: String
}

interface B {
  b: String
}

type C implements A & A & B {
  a: String
  b: String
}

extend type C implements A {
  c: String
}
`;

const schemaWithEnumDesc = `
"Enum type description"
enum Foo {
  """
  Enum field description
  """
  BAR
}
`;

describe("Default options", () => {
  it("should convert valid schemas in convertAppSyncSchemas().", async () => {
    expect(() => convertAppSyncSchemas([""])).not.toThrow();
  });

  it("should convert valid schema in convertSchemas().", async () => {
    expect(() => convertSchemas("")).not.toThrow();
  });
});

describe("Scalars and directives", () => {
  it("should not leak default internal definitions.", async () => {
    expect(await convertSchemas("")).toBe("");
  });

  it("should retain AppSync scalars.", async () => {
    const emptySchema = await buildSchema("");
    const scalars = Object.keys(emptySchema.getTypeMap()).filter((__typename) => /^AWS/.test(__typename));
    const scalarsSDL = scalars.map((scalar) => `${scalar.replace(/^AWS/, "").toLowerCase()}: ${scalar}`);
    const schema = await convertSchemas(
      [
        baseSchema,
        `
          extend type Query {
            ${scalarsSDL.join("\n")}
          }
        `,
      ],
      { commentDescriptions: true },
    );

    expect(schema).toMatchSnapshot();
  });

  it("should retain AppSync directives.", async () => {
    const schema = await convertSchemas([baseSchema, schemaWithDirectives], {
      commentDescriptions: true,
      includeDirectives: true,
    });

    expect(schema).toMatchSnapshot();
  });
});

describe("Descriptions", () => {
  it("should be converted from literal descriptions into comment descriptions.", async () => {
    const schema = await convertSchemas(baseSchema, {
      commentDescriptions: true,
    });

    expect(/# type description/.test(schema) && /# field description/.test(schema)).toBe(true);
  });
});

describe("Schemas", () => {
  it("should merge type extensions.", async () => {
    const schema = await convertSchemas([baseSchema, extendedSchema], {
      commentDescriptions: true,
    });

    expect(schema).toMatchSnapshot();
  });

  it("should apply custom interface separators.", async () => {
    const schema = await convertSchemas([baseSchema, schemaWithInterfaces], {
      commentDescriptions: true,
      includeDirectives: true,
      interfaceSeparator: ", ",
    });

    expect(schema).toMatchSnapshot();
  });

  it("should dedupe interfaces even through extension.", async () => {
    const schema = await convertSchemas([baseSchema, schemaWithDuplicatedInterfacesViaExtension], {
      commentDescriptions: true,
      includeDirectives: true,
      interfaceSeparator: ", ",
    });

    expect(schema).toMatchSnapshot();
  });
});

describe("Enumerations", () => {
  it("should retain descriptions by default.", async () => {
    const schema = await convertSchemas(schemaWithEnumDesc, {
      commentDescriptions: true,
    });

    expect(schema).toMatchSnapshot();
  });

  it("should removes descriptions.", async () => {
    const schema = await convertSchemas(schemaWithEnumDesc, {
      commentDescriptions: true,
      includeEnumDescriptions: false,
    });

    expect(schema).toMatchSnapshot();
  });
});
