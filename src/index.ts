import {
  buildASTSchema,
  DefinitionNode,
  extendSchema,
  GraphQLDirective,
  GraphQLType,
  isScalarType,
  isSpecifiedDirective,
  Kind,
  parse,
} from "graphql";
import AppSyncDirectives from "./directives.graphql";
import { isDefinedType, PrintOptions, printSchema } from "./printSchema";
import AppSyncScalars from "./scalars.graphql";

/**
 * Do not print @aws_* directives
 */
export const defaultDirectiveFilter = (directive: GraphQLDirective) =>
  !isSpecifiedDirective(directive) && !/^aws_/.test(directive.name) && directive.name !== "defer";

/**
 * Do not print AWS* scalars
 */
export const defaultTypeFilter = (type: GraphQLType) =>
  isDefinedType(type) && !(isScalarType(type) && /^AWS/.test(type.name));

/**
 * Simplified version of `convertSchemas`, with AppSync compatible options
 * specified.
 */
export const convertAppSyncSchemas = (schemas: string[]) =>
  convertSchemas(schemas, {
    commentDescriptions: true,
    includeDirectives: true,
    includeEnumDescriptions: false,
    interfaceSeparator: ", ",
    directiveFilter: defaultDirectiveFilter,
    typeFilter: defaultTypeFilter,
  });

/**
 * Convert Apollo GraphQL schemas into AppSync verison, following internal
 * defaults of `graphql/utilities/printSchema.js`.
 */
export const convertSchemas = (
  schemas: string | string[],
  {
    commentDescriptions = false,
    includeDirectives = false,
    includeEnumDescriptions = true,
    interfaceSeparator = " & ",
    directiveFilter = defaultDirectiveFilter,
    typeFilter = defaultTypeFilter,
  }: PrintOptions = {},
) => {
  const schema = buildSchema(Array.isArray(schemas) ? schemas.join("\n") : schemas);

  return printSchema(schema, {
    commentDescriptions,
    includeDirectives,
    includeEnumDescriptions,
    interfaceSeparator,
    directiveFilter,
    typeFilter,
  })
    .replace(AppSyncScalars, "")
    .replace(AppSyncDirectives, "")
    .trim();
};

export const buildSchema = (schemas: string) => {
  const [defs, exts] = parse([AppSyncScalars, AppSyncDirectives].concat(schemas).join("\n")).definitions.reduce<
    [DefinitionNode[], DefinitionNode[]]
  >(
    ([defs, exts], astNode) => {
      (/Extension$/.test(astNode.kind) ? exts : defs).push(astNode);
      return [defs, exts];
    },
    [[], []],
  );

  // buildSchema() does not handles type extension, extendSchema() is required.
  return extendSchema(buildASTSchema({ kind: Kind.DOCUMENT, definitions: defs }), {
    kind: Kind.DOCUMENT,
    definitions: exts,
  });
};

export { default as AppSyncDirectives } from "./directives.graphql";
export { printSchema, printType } from "./printSchema";
export type { printIntrospectionSchema } from "./printSchema";
export { default as AppSyncScalars } from "./scalars.graphql";
