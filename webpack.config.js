const path = require("path");
const nodeExternals = require("webpack-node-externals");
const ShebangPlugin = require("webpack-shebang-plugin");

module.exports = {
  context: path.resolve("./src"),
  entry: {
    index: "./index.ts",
    cli: "./cli.ts",
  },
  target: "node",
  devtool: "source-map",
  externals: [nodeExternals({ modulesFromFile: true })],
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: "ts-loader",
        options: {
          configFile: "tsconfig.json",
        },
      },
      {
        test: /\.(graphql|gql)$/,
        use: "raw-loader",
      },
    ],
  },
  plugins: [new ShebangPlugin()],
  resolve: {
    extensions: [".tsx", ".ts", ".js", ".jsx"],
  },
  output: {
    library: {
      type: "commonjs",
    },
  },
};
